import pandas as pd
from sklearn.svm import SVC
from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score

#Getting data
df = pd.read_csv("resources/smallTr.csv", index_col=0, parse_dates=True)
#print(pd.DataFrame.head(df)) #head of dataframe

#Feature normalization and preprocessing
dfX = df.drop(["Y"], axis=1)
#print(pd.DataFrame.head(dfX))
dfY = df["Y"]
dfX_norm = preprocessing.normalize(dfX, norm='l2')
#print(df.columns.values) #column names
#print(pd.DataFrame.head(df.drop(["Y"],axis=1))) #select all except Y col
#head = dfX_norm[1:6, :]
#print(head)

#Splitting into training and test set
X_train, X_test, y_train, y_test = train_test_split(dfX_norm, dfY, test_size=0.33, random_state=42)

#Building SVC
model = SVC()
model.fit(X_train, y_train)
y_pred = model.predict(X_train)
print(accuracy_score(y_train, y_pred))
print(model.score(X_train, y_train))
print(model.score(X_test, y_test))

#model.predict(X_train)
#print(model.predict())



